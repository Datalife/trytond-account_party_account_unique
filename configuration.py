# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.modules.company.model import CompanyValueMixin
from trytond import backend
from trytond.tools.multivalue import migrate_property

__all__ = ['Configuration', 'ConfigurationAccount']
account_names = [
    'account_view_payable', 'account_view_payable_creditor',
    'account_view_receivable', 'account_view_payable_sequence',
    'account_view_payable_creditor_sequence',
    'account_view_receivable_sequence']

__all__ = ['Configuration']


def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration:
    __name__ = 'account.configuration'
    __metaclass__ = PoolMeta

    account_view_payable = fields.MultiValue(
        fields.Many2One('account.account', 'Account view Payable',
            domain=[('kind', '=', 'view'),
                    ('company', '=', Eval('context', {}).get('company', -1))])
    )
    account_view_payable_creditor = fields.MultiValue(
        fields.Many2One('account.account', 'Account view Payable creditor',
            domain=[('kind', '=', 'view'),
                    ('company', '=', Eval('context', {}).get('company', -1))])
    )
    account_view_receivable = fields.MultiValue(
        fields.Many2One('account.account', 'Account view Receivable',
            domain=[('kind', '=', 'view'),
                    ('company', '=', Eval('context', {}).get('company', -1))])
    )
    account_view_payable_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Account view payable Sequence',
                        domain=[('code', '=', 'account.account')])
    )
    account_view_payable_creditor_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence',
            'Account view payable creditor Sequence',
            domain=[('code', '=', 'account.account')])
    )
    account_view_receivable_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence',
            'Account view receivable Sequence',
            domain=[('code', '=', 'account.account')])
    )

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in account_names:
            return pool.get('account.configuration.account_view')
        return super(Configuration, cls).multivalue_model(field)

    default_account_view_payable_sequence = default_func(
        'account_view_payable_sequence')
    default_account_view_payable_creditor_sequence = default_func(
        'account_view_payable_creditor_sequence')
    default_account_view_receivable_sequence = default_func(
        'account_view_receivable_sequence')


class ConfigurationAccount(ModelSQL, CompanyValueMixin):
    "Account Configuration Account View"
    __name__ = 'account.configuration.account_view'

    account_view_payable = fields.Many2One('account.account',
        'Account view Payable',
        domain=[
            ('kind', '=', 'view'),
            ('company', '=', Eval('context', {}).get('company', -1))
            ])
    account_view_payable_creditor = fields.Many2One('account.account',
        'Account view Payable creditor',
        domain=[
            ('kind', '=', 'view'),
            ('company', '=', Eval('context', {}).get('company', -1))
            ])
    account_view_receivable = fields.Many2One('account.account',
        'Account view Receivable',
        domain=[
            ('kind', '=', 'view'),
            ('company', '=', Eval('context', {}).get('company', -1))
            ])
    account_view_payable_sequence = fields.Many2One('ir.sequence',
        'Account view payable Sequence',
        domain=[
            ('code', '=', 'account.account')])
    account_view_payable_creditor_sequence = fields.Many2One('ir.sequence',
            'Account view payable creditor Sequence',
            domain=[
                ('code', '=', 'account.account')
                ])
    account_view_receivable_sequence = fields.Many2One('ir.sequence',
            'Account view receivable Sequence',
            domain=[
                ('code', '=', 'account.account')
                ])

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        exist = TableHandler.table_exist(cls._table)

        super(ConfigurationAccount, cls).__register__(module_name)
        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.extend(account_names)
        value_names.extend(account_names)
        fields.append('company')
        migrate_property('account.configuration',
            field_names, cls, value_names, fields=fields)

    @classmethod
    def default_account_view_payable_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('account_party_account_unique',
                'sequence_payable_account')
        except KeyError:
            return None

    @classmethod
    def default_account_view_payable_creditor_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('account_party_account_unique',
                'sequence_payable_creditor_account')
        except KeyError:
            return None

    @classmethod
    def default_account_view_receivable_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('account_party_account_unique',
                'sequence_receivable_account')
        except KeyError:
            return None

